package parivar.accounting;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import parivar.accounting.db.DB;
import parivar.accounting.db.FirebaseDB;
import parivar.accounting.db.TaskComplete;
import parivar.accounting.util.BudgetType;
import parivar.accounting.util.CurrencyExchangeService;
import parivar.accounting.util.CurrencyHelper;
import parivar.accounting.util.CurrencyTaskComplete;

import static android.content.ContentValues.TAG;


public class MainActivity extends Activity implements OnClickListener, TaskComplete, CurrencyTaskComplete {

    final String LOG_TAG = "myLogs";


    Button btnAdd, btnSub, btnShow, btnClear, btnMenu, btnCategory, btnMain, btnRealBalance, btnSettings;
    TextView tvSum;
    LinearLayout linLayout, layoutMenu;
    RelativeLayout mainLayout;

    boolean flagHide;
    String moneyItem;

    DB db;
    FirebaseDB firebaseDB;
    CurrencyExchangeService currencyExchangeService;
    LayoutInflater ltInflater;
    Double currencyValue = 1.0;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        initUI();
        ltInflater = getLayoutInflater();
        firebaseDB = new FirebaseDB(this);
        firebaseDB.getAllBudgets();
        currencyExchangeService = new CurrencyExchangeService(this);
        currencyExchangeService.execute();
        //currencyExchangeService.execute();
        //readValues();
    }

    private void initUI() {
        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);

        btnSub = (Button) findViewById(R.id.btnSub);
        btnSub.setOnClickListener(this);

        btnShow = (Button) findViewById(R.id.btnShow);
        btnShow.setOnClickListener(this);

        btnClear = (Button) findViewById(R.id.btnClear);
        btnClear.setOnClickListener(this);

        btnMenu = (Button) findViewById(R.id.btnMenu);
        btnMenu.setOnClickListener(this);

        btnCategory = (Button) findViewById(R.id.btnCategory);
        btnCategory.setOnClickListener(this);

        btnMain = (Button) findViewById(R.id.btnMain);
        btnMain.setOnClickListener(this);

        btnRealBalance = (Button) findViewById(R.id.btnRealBalance);
        btnRealBalance.setOnClickListener(this);

        btnSettings = (Button) findViewById(R.id.btnSettings);
        btnSettings.setOnClickListener(this);

        tvSum = (TextView) findViewById(R.id.sumView);

        linLayout = (LinearLayout) findViewById(R.id.linLayout);
        layoutMenu = (LinearLayout) findViewById(R.id.layoutMenu);
        mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(currencyExchangeService.getStatus() != AsyncTask.Status.RUNNING){
            currencyExchangeService = new CurrencyExchangeService(this);
            currencyExchangeService.execute();
        }
       // firebaseDB.getAllBudgets();
       // readValues();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAdd:
                //TODO delete tybyInputBool
//                Intent intentAdd = new Intent(this, InputActivity.class);
                Intent intentAdd = new Intent(this, InputActivity.class);
                intentAdd.putExtra("typeInput", getString(R.string.input_add));
                intentAdd.putExtra("typeInputBool", "true");
                intentAdd.putExtra("currencyValue", currencyValue);
                startActivity(intentAdd);
                break;
            case R.id.btnSub:
//                Intent intentSub = new Intent(this, InputActivity.class);
                Intent intentSub = new Intent(this, InputActivity.class);
                intentSub.putExtra("typeInput", getString(R.string.input_sub));
                intentSub.putExtra("typeInputBool", "false");
                intentSub.putExtra("currencyValue", currencyValue);
                startActivity(intentSub);
                break;
            case R.id.btnClear:
                //db.delAllRec();
                firebaseDB.deleteAllRecords();
                //firebaseDB.getAllBudgets();
                //readValues();
                break;
            case R.id.btnShow:
                if (!flagHide) {
                    firebaseDB.getAllBudgets();
                    //readValues();
                } else {
                    linLayout.removeAllViews();
                    flagHide = false;
                }
                break;
            case R.id.btnMenu:
                firebaseDB.getAllBudgets();
                RelativeLayout.LayoutParams layoutMenuParams = (RelativeLayout.LayoutParams) layoutMenu.getLayoutParams();

                if (layoutMenuParams.width != 0) {
                    layoutMenuParams.width = 0;
                } else {
                    layoutMenuParams.width = (int) getResources().getDimension(R.dimen.menu_size);
                }
                layoutMenu.setLayoutParams(layoutMenuParams);

                break;
            case R.id.btnCategory:
//                mainLayout.removeAllViews();
//                LayoutInflater ltInflater = getLayoutInflater();
//                final View item = ltInflater.inflate(R.layout.category_list_edit, mainLayout, false);
//                mainLayout.addView(item);
                Intent categoryIntent = new Intent(this, CategoryActivity.class);
                startActivity(categoryIntent);
                break;
            case R.id.btnMain:
//                mainLayout.removeAllViews();
//                onCreate(null);
                break;
            case R.id.btnRealBalance:
                Intent intentRealBalance = new Intent(this, InputBalance.class);
                startActivity(intentRealBalance);
                break;
            case R.id.btnSettings:
                Intent intentSettings = new Intent(this, CurrencyChoiceActivity.class);
                intentSettings.putExtra("typeInput", getString(R.string.input_add));
                startActivity(intentSettings);
                break;


        }
    }


    @Override
    public void onGetAllBudgets(Task<QuerySnapshot> task) {


        linLayout.removeAllViews();
        int balance = 0;
        Double totalBalance = 1.0;
        if(task == null) {
            tvSum.setText("0");
            return;
        }
        for (QueryDocumentSnapshot document : task.getResult()) {

            String id = document.getId();
            Double sum = (Double)document.get(FirebaseDB.DB_BUDGET_COLUMN_SUM);
            String typeStr = (String)document.get(FirebaseDB.DB_BUDGET_COLUMN_TYPE);
            BudgetType type = BudgetType.valueOf(typeStr.toUpperCase());
            String date = (String)document.get(FirebaseDB.DB_BUDGET_COLUMN_DATE);
            String category = (String)document.get(FirebaseDB.DB_BUDGET_COLUMN_CATEGORY_NAME);

            Log.d(TAG, document.getId() + " => " + document.getData());
            final View item = ltInflater.inflate(R.layout.item, linLayout, false);
            TextView tvDate = (TextView) item.findViewById(R.id.tvDate);
            TextView tvMoney = (TextView) item.findViewById(R.id.tvMoney);
            TextView tvCategory = (TextView) item.findViewById(R.id.tvCategory);
            TextView tvId = (TextView) item.findViewById(R.id.tvId);

            tvDate.setText(getString(R.string.v_date) + ": " + date);

            Double finalVal = currencyValue*sum;
            if (type == BudgetType.ADD) {
                tvMoney.setText(getString(R.string.v_add) + ": " + finalVal + " " + CurrencyHelper.currency.toString());
                item.setBackgroundColor(getResources().getColor(R.color.add));
                moneyItem = "" + sum;
                balance += sum;
            } else {
                tvMoney.setText(getString(R.string.v_sub) + ": " + finalVal + " " + CurrencyHelper.currency.toString());
                item.setBackgroundColor(getResources().getColor(R.color.sub));
                moneyItem = "" + sum;
                balance -= sum;
            }


            tvCategory.setText(getString(R.string.v_category) + ": " + category);

            tvId.setText(id);

            item.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
            linLayout.addView(item, 0);

            item.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    TextView tvId = (TextView) v.findViewById(R.id.tvId);
                    Intent intentContext = new Intent(MainActivity.this, ContextMenuActivity.class);
                    intentContext.putExtra("idItem", "" + tvId.getText());
                    startActivity(intentContext);
                }
            });
        }
        totalBalance= currencyValue*balance;
        tvSum.setText(String.valueOf(totalBalance) + " " + CurrencyHelper.currency.toString());
        flagHide = true;
    }

    @Override
    public void onGetBudget(DocumentSnapshot snapshot) {

    }

    @Override
    public void onGetAllCategories(Task<QuerySnapshot> task) {

    }

    @Override
    public void onGetTotalBalance(Double total) {

    }

    @Override
    public void onCurrencyGet(String get) {
        currencyValue = Double.valueOf(get);
        firebaseDB.getAllBudgets();
       // Log.d(LOG_TAG, get);
    }




}