package parivar.accounting;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import parivar.accounting.db.FirebaseDB;
import parivar.accounting.db.TaskComplete;
import parivar.accounting.util.BudgetType;
import parivar.accounting.util.Currency;
import parivar.accounting.util.CurrencyHelper;

public class InputActivity extends Activity implements View.OnClickListener, TaskComplete {

    TextView tvHeadInput, tvEnterCategory;
    EditText etMoney;
    Spinner spinner;
    Button btnInput;

    String category, categoryItem, moneyItem, typeItem, idItem, editItem;

    int ColId;
    Double currencyValue;

    boolean typeInputBool;

    final String LOG_TAG = "myLogs";

    //DB db;
    FirebaseDB firebaseDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.input_money);

        firebaseDB = new FirebaseDB(this);
        firebaseDB.getAllCategories();

        initUI();
        //TODO переписать на передачу одного параметра интенту
        Intent intent = getIntent();
        tvHeadInput.setText(intent.getStringExtra("typeInput"));
        moneyItem = intent.getStringExtra("moneyItem");
        categoryItem = intent.getStringExtra("categoryItem");
        typeItem = intent.getStringExtra("typeItem");
        idItem = intent.getStringExtra("idItem");
        editItem = intent.getStringExtra("editItem");
        currencyValue = intent.getDoubleExtra("currencyValue", 1.0);


        if (moneyItem != null) {
            etMoney.setText(moneyItem);
        }

        if (editItem != null) {
            btnInput.setText(getString(R.string.edit_btn));
        }

        if (categoryItem != null) {
           // makeSpinner(categoryItem);

        } else {
            categoryItem = "0";
            //makeSpinner("0");
        }

        if (typeItem != null && typeItem.equals("1")) {
            typeInputBool = true;
        } else if (typeItem != null && typeItem.equals("0")) {
            typeInputBool = false;
        } else {
            typeInputBool = Boolean.parseBoolean(intent.getStringExtra("typeInputBool"));
        }
    }

    private void initUI() {
        tvHeadInput = (TextView) findViewById(R.id.tvHeadInput);
        tvEnterCategory = (TextView) findViewById(R.id.tvEnterCategory);
        etMoney = (EditText) findViewById(R.id.etMoney);
        spinner = (Spinner) findViewById(R.id.spinner);

        btnInput = (Button) findViewById(R.id.btnInput);
        btnInput.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnInput:
                /*db = new DB(this);
                db.open();*/
                Double amount = returnMoney();
                if (amount == new Double(0)) break;
                if(CurrencyHelper.currency != Currency.EUR){
                    amount = amount.doubleValue()/currencyValue;
                }

                if (editItem != null) {
                    firebaseDB.updateBudget(idItem, amount, category);
                    //db.updateValue(idItem, "" + returnMoney(), "0", " \" " + category + " \" ");
                } else if (typeInputBool) {
                    firebaseDB.addBudget(amount, BudgetType.ADD, returnDate(), category);
                    //db.addRec(returnDate(), returnMoney(), 0, category);
                } else {
                    firebaseDB.addBudget(amount, BudgetType.SUB, returnDate(), category);
                   // db.addRec(returnDate(), 0, returnMoney(), category);
                }
                spinner.setSelection(0); //TODO попробовать убрать ???
                //db.close();
                hideKeyboard();
                finish();
                break;
        }
    }

    public String returnDate() {
        Date d = new Date();
        SimpleDateFormat fromUser = new SimpleDateFormat("dd.MM.yyyy");
        return fromUser.format(d);
    }

    public Double returnMoney() {
        String amount = etMoney.getText().toString();

        if (amount.trim().isEmpty()) return new Double(0);
        else return new Double(amount);
    }

    private void makeSpinner(String copyPos) {

    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    @Override
    public void onGetAllBudgets(Task<QuerySnapshot> task) {

    }

    @Override
    public void onGetBudget(DocumentSnapshot snapshot) {

    }

    @Override
    public void onGetAllCategories(Task<QuerySnapshot> task) {

        final Map<Integer, String> categoryMapCollection = new HashMap();
        ColId = 0;
        for (QueryDocumentSnapshot document : task.getResult()) {
            String name = (String)document.get(FirebaseDB.DB_CATEGORY_COLUMN_NAME);
            categoryMapCollection.put(ColId++, name);
        }


        final List<String> categoryList = new ArrayList<>(categoryMapCollection.values());

        if (task.getResult().isEmpty()) {
            spinner.setVisibility(View.INVISIBLE);
            tvEnterCategory.setVisibility(View.INVISIBLE);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, categoryList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
        spinner.setPrompt(getString(R.string.v_category));
        if (!categoryItem.equals("0")) {
            Log.v(LOG_TAG, "if (!copyPos.equals(\"0\")) {");
            for (Map.Entry<Integer, String> entry : categoryMapCollection.entrySet()) {
                if (categoryItem.equals(entry.getValue())) spinner.setSelection(entry.getKey());
                Log.v(LOG_TAG, "set selection " + entry.getKey());

            }

        } else {
            spinner.setSelection(0);
            Log.v(LOG_TAG, "tutu");
        }

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                category = categoryMapCollection.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    @Override
    public void onGetTotalBalance(Double total) {

    }


}
