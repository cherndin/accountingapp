package parivar.accounting.db;


import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.Transaction;
import com.google.firebase.firestore.WriteBatch;

import java.util.HashMap;
import java.util.Map;

import parivar.accounting.util.BudgetType;

import static android.content.ContentValues.TAG;


public class FirebaseDB {

    public static String DB_BUDGET = "budget";
    public static String DB_CATEGORY = "category";

    public static String DB_BUDGET_COLUMN_ID = "id";
    public static String DB_BUDGET_COLUMN_SUM = "sum";
    public static String DB_BUDGET_COLUMN_TYPE = "type";
    public static String DB_BUDGET_COLUMN_DATE = "date";
    public static String DB_BUDGET_COLUMN_CATEGORY_ID = "category_id";
    public static String DB_BUDGET_COLUMN_CATEGORY_NAME = "category_name";

    public static String DB_CATEGORY_COLUMN_ID = "_id";
    public static String DB_CATEGORY_COLUMN_NAME = "name";



    private FirebaseFirestore mDatabase;
    private TaskComplete context;

    public FirebaseDB(Context context){
        mDatabase = FirebaseFirestore.getInstance();
        //addCategoryAndBudget();
        //addBudget(2000, BudgetType.ADD, "12.23.45", "flat");
        this.context = (TaskComplete)context;
    }


    public void addBudget(Double sum, BudgetType type, String date, String category){
        // Create a new user with a first and last name
        Map<String, Object> budget = new HashMap<>();

        budget.put(DB_BUDGET_COLUMN_SUM, sum);
        budget.put(DB_BUDGET_COLUMN_TYPE, type.toString());
        budget.put(DB_BUDGET_COLUMN_DATE, date);
        budget.put(DB_BUDGET_COLUMN_CATEGORY_NAME, category);

        // Add a new document with a generated ID
        mDatabase.collection("budget")
                .add(budget)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d(TAG, "DocumentSnapshot added with ID: " + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error adding document", e);
                    }
                });
    }

    public void addCategory(String name){
        // Create a new user with a first and last name
        Map<String, Object> category = new HashMap<>();

        //category.put(DB_CATEGORY_COLUMN_ID, "_" + name);
        category.put(DB_CATEGORY_COLUMN_NAME, name);

        // Add a new document with a generated ID
        mDatabase.collection("category").document("_" + name)
                .set(category)
                .addOnSuccessListener(
                        new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d(TAG, "DocumentSnapshot successfully written!");
            }
             }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error writing document", e);
                    }
                });

        }


        public void getAllBudgets(){
            mDatabase.collection(DB_BUDGET).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        context.onGetAllBudgets(task);
                        Double total = new Double(0);
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Log.d(TAG, document.getId() + " => " + document.getData());

                            Double sum = (Double)document.get(FirebaseDB.DB_BUDGET_COLUMN_SUM);
                            String typeStr = (String)document.get(FirebaseDB.DB_BUDGET_COLUMN_TYPE);
                            BudgetType type = BudgetType.valueOf(typeStr.toUpperCase());

                            if (type == BudgetType.ADD) {
                                total += sum;
                            } else if(type == BudgetType.SUB){
                                total -= sum;
                            }

                        }
                        context.onGetTotalBalance(total);
                    } else {
                        Log.w(TAG, "Error getting documents.", task.getException());
                    }
                }
            });
        }



        public void updateBudget(String id, final Double sum, final String category){
            final DocumentReference sfDocRef = mDatabase.collection(DB_BUDGET).document(id);

            mDatabase.runTransaction(new Transaction.Function<Void>() {
                @Override
                public Void apply(Transaction transaction) throws FirebaseFirestoreException {
                    // update sum
                    Double newSum = sum.doubleValue();
                    transaction.update(sfDocRef, DB_BUDGET_COLUMN_SUM, newSum);
                    // update category
                    String newCategory = category;
                    transaction.update(sfDocRef, DB_BUDGET_COLUMN_CATEGORY_NAME, newCategory);

                    // Success
                    return null;
                }
            }).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Log.d(TAG, "Transaction success!");
                }
            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w(TAG, "Transaction failure.", e);
                        }
                    });
        }

        public void getBudget(String id){
            DocumentReference docRef = mDatabase.collection(DB_BUDGET).document(id);
            docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            context.onGetBudget(document);
                            Log.d(TAG, "Budget data: " + document.getData());
                        } else {
                            Log.d(TAG, "No such document");
                        }
                    } else {
                        Log.d(TAG, "get failed with ", task.getException());
                    }
                }
            });
        }

    public void getCategory(String id){
        DocumentReference docRef = mDatabase.collection(DB_CATEGORY).document(id);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        //context.onGetCategory(document);
                        Log.d(TAG, "Category data: " + document.getData());
                    } else {
                        Log.d(TAG, "No such document");
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });
    }


    public void getAllCategories(){
        mDatabase.collection(DB_CATEGORY).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                context.onGetAllCategories(task);
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        Log.d(TAG, document.getId() + " => " + document.getData());
                    }
                } else {
                    Log.w(TAG, "Error getting documents.", task.getException());
                }
            }
        });
    }

    public void removeCategory(String id){
        mDatabase.collection(DB_CATEGORY).document(id)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        //context.onCategoryRemoved();
                        Log.d(TAG, "DocumentSnapshot successfully deleted!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error deleting document", e);
                    }
                });
    }


    public void deleteAllRecords(){
       /* WriteBatch batch = mDatabase.batch();
        mDatabase.collection().whereEqualTo(...).get().result.forEach {
            batch.delete(it.reference)
        }
        batch.commit()*/


        mDatabase.collection(DB_BUDGET).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                //context.onGetAllCategories(task);
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        String id = document.getId();

                        // remove every single one
                        mDatabase.collection(DB_BUDGET).document(id)
                                .delete()
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Log.d(TAG, "DocumentSnapshot successfully deleted!");
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.w(TAG, "Error deleting document", e);
                                    }
                                });

                        Log.d(TAG, document.getId() + " => " + document.getData());
                    }
                    context.onGetAllBudgets(null);
                } else {
                    Log.w(TAG, "Error getting documents.", task.getException());
                }
            }
        });
    }


    public void deleteBudget(String id){
        mDatabase.collection(DB_BUDGET).document(id)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot successfully deleted!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error deleting document", e);
                    }
                });
    }

}


