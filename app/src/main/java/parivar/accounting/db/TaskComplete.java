package parivar.accounting.db;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public interface TaskComplete {
    public void onGetAllBudgets(Task<QuerySnapshot> task);

    public void onGetBudget(DocumentSnapshot snapshot);

    public void onGetAllCategories(Task<QuerySnapshot> task);

    public void onGetTotalBalance(Double total);
}
