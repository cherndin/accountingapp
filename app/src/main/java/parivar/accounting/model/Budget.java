package parivar.accounting.model;


import parivar.accounting.util.BudgetType;

public class Budget {

    public int sum;
    public BudgetType type;
    public Category category;

    public Budget(){

    }

    public Budget(int sum, BudgetType type, Category category){
        this.sum = sum;
        this.type = type;
        this.category = category;
    }
}
