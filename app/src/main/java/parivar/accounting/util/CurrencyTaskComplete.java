package parivar.accounting.util;


import com.google.gson.JsonElement;

public interface CurrencyTaskComplete {

    public abstract void onCurrencyGet(String get);
}
