package parivar.accounting.util;

public enum Currency {
    EUR, CZK, RSD, RUB
}
