package parivar.accounting.util;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class CurrencyExchangeService extends AsyncTask<Void, Void, String> {

    private final String API_KEY = "d061c3fcd28b1881aff676f17be2d5c2";
    private final String TAG = "CurrencyExchangeService";
    private CurrencyTaskComplete context;


    public CurrencyExchangeService(CurrencyTaskComplete context){
        this.context = context;
    }


    @Override
    protected String doInBackground(Void... voids) {
        Currency val = CurrencyHelper.currency;
        String resp = getResponse();
        if(resp == null){
            return "1.0";
        }
        JsonElement jsonElement = parse(resp);
        String res = parseJson(jsonElement);
        return res;
    }

    private String parseJson(JsonElement jsonElement){
        String res = "";
        JsonObject jsonObject = jsonElement.getAsJsonObject();

        JsonElement rates = jsonObject.get("rates");
        if(rates.isJsonObject()){
            JsonObject ratesAsJsonObject = rates.getAsJsonObject();

            JsonElement neededCurrency = ratesAsJsonObject.get(CurrencyHelper.currency.toString());
            res = neededCurrency.getAsString();
        }
        return res;
    }

    private String getResponse(){
        String out = "";
        try {
            String path = "http://data.fixer.io/api/latest?access_key=" + API_KEY;

            if(CurrencyHelper.currency == Currency.EUR){
                return null;
            } else {
                path += "&base=" + Currency.EUR + "&symbols=" + CurrencyHelper.currency.toString();
            }

            URL url = new URL(path);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output = "X3";
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                out = output;
                Log.d(TAG, output);
                System.out.println(output);
            }

            conn.disconnect();

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }


        return out;
    }


    @Override
    protected void onPostExecute(String result) {
        context.onCurrencyGet(result);
    }

    private JsonElement parse(String json){
        JsonParser parser = new JsonParser();
       return parser.parse(json);
    }

}
