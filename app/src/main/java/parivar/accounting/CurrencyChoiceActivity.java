package parivar.accounting;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.gson.JsonElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import parivar.accounting.db.FirebaseDB;
import parivar.accounting.util.Currency;
import parivar.accounting.util.CurrencyExchangeService;
import parivar.accounting.util.CurrencyHelper;
import parivar.accounting.util.CurrencyTaskComplete;

public class CurrencyChoiceActivity extends Activity implements View.OnClickListener {


    private TextView tvHeadCurrency, tvEnterCurrency, btnAddCurrency;
    private Spinner currencySpinner;
    //private FirebaseDB firebaseDB;
    //private CurrencyExchangeService currencyExchangeService;
    private List<Currency> currencies;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.currency_choice);
        //firebaseDB = new FirebaseDB(this);
        //currencyExchangeService = new CurrencyExchangeService(this);

        initUI();

        Intent intent = getIntent();
        tvHeadCurrency.setText(intent.getStringExtra("typeInput"));

        //CurrencyHelper.currency = null;
        setCurrencies();
        chooseCurrency();

    }

    private void setCurrencies(){
        currencies = new ArrayList<>();
        currencies.add(Currency.EUR);
        currencies.add(Currency.CZK);
        currencies.add(Currency.RSD);
        currencies.add(Currency.RUB);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAddCurrency:
                //currencyExchangeService.execute();
                //chooseCurrency();
                //hideKeyboard();
                finish();
                break;
        }
    }


    private void initUI() {
        tvHeadCurrency = (TextView) findViewById(R.id.tvHeadCurrency);
        tvEnterCurrency = (TextView) findViewById(R.id.tvEnterCurrency);
        currencySpinner = (Spinner) findViewById(R.id.currencySpinner);
        btnAddCurrency = (Button) findViewById(R.id.btnAddCurrency);
        btnAddCurrency.setOnClickListener(this);
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }


    public void chooseCurrency() {
        final Map<Integer, String> currencyMapCollection = new HashMap();
        int ColId = 0;
        for (Currency currency : currencies) {
            currencyMapCollection.put(ColId++, currency.toString());
        }

        ArrayAdapter<Currency> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, currencies);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        currencySpinner.setAdapter(adapter);

        currencySpinner.setPrompt(getString(R.string.v_currency));

        currencySpinner.setSelection(0);

        currencySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String curString = currencyMapCollection.get(position);
                if(curString == null || curString == "") CurrencyHelper.currency = Currency.EUR;
                else CurrencyHelper.currency = Currency.valueOf(curString.toUpperCase());
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });



    }
}
