package parivar.accounting;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import parivar.accounting.db.DB;
import parivar.accounting.db.FirebaseDB;
import parivar.accounting.db.TaskComplete;
import parivar.accounting.util.BudgetType;

public class InputBalance extends Activity implements View.OnClickListener, TaskComplete {
    Button btnRealBtnOk;
    EditText etRealBalance;
    DB db;
    FirebaseDB firebaseDB;
    Double readBalanceValue;

    Double totalBalance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.input_real_balance);

        btnRealBtnOk = (Button) findViewById(R.id.btnRealBtnOk);
        btnRealBtnOk.setOnClickListener(this);

        etRealBalance = (EditText) findViewById(R.id.etRealBalance);
        firebaseDB = new FirebaseDB(this);
        firebaseDB.getAllBudgets();
    }

    @Override
    public void onClick(View v) {
       /* db = new DB(this);
        db.open();*/

        switch (v.getId()) {
            case R.id.btnRealBtnOk:
                if (etRealBalance.getText() != null) {
                    readBalanceValue = Double.valueOf(etRealBalance.getText().toString());
                    if(totalBalance == null){
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    Date c = Calendar.getInstance().getTime();
                    SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
                    String formattedDate = df.format(c);

                    Double sub = readBalanceValue - totalBalance;
                    if (sub > 0) {
                        firebaseDB.addBudget(sub, BudgetType.ADD, formattedDate, getResources().getString(R.string.inputRealStateOfBalanse));
                        //db.addRec(null, sub, 0, getResources().getString(R.string.inputRealStateOfBalanse));
                    } else {
                        firebaseDB.addBudget(sub, BudgetType.SUB, formattedDate, getResources().getString(R.string.inputRealStateOfBalanse));
                        //db.addRec(null, 0, -1 * sub, getResources().getString(R.string.inputRealStateOfBalanse));
                    }

                }
               //db.close();
                hideKeyboard();
                finish();

        }
    }
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    @Override
    public void onGetAllBudgets(Task<QuerySnapshot> task) {

    }

    @Override
    public void onGetBudget(DocumentSnapshot snapshot) {

    }

    @Override
    public void onGetAllCategories(Task<QuerySnapshot> task) {

    }

    @Override
    public void onGetTotalBalance(Double total) {
        totalBalance = total;
    }
}
