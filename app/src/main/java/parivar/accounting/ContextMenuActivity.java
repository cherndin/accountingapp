package parivar.accounting;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import parivar.accounting.db.DB;
import parivar.accounting.db.FirebaseDB;
import parivar.accounting.db.TaskComplete;
import parivar.accounting.util.BudgetType;

public class ContextMenuActivity extends Activity implements View.OnClickListener, TaskComplete {

    Button btnEdit, btnCopy, btnDel, btnBack;

    String idItem, moneyItem, categoryItem, typeItem;

    DB db;
    FirebaseDB firebaseDB;

    final String LOG_TAG = "myLogs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.context_menu);

        firebaseDB = new FirebaseDB(this);

        btnEdit = (Button) findViewById(R.id.btnEdit);
        btnEdit.setOnClickListener(this);

        btnCopy = (Button) findViewById(R.id.btnCopy);
        btnCopy.setOnClickListener(this);

        btnDel = (Button) findViewById(R.id.btnDel);
        btnDel.setOnClickListener(this);

        btnBack = (Button) findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);

        /*db = new DB(this);
        db.open();*/

        Intent intent = getIntent();
        idItem = intent.getStringExtra("idItem");

        LayoutInflater ltInflater = getLayoutInflater();
        LinearLayout linLayout = (LinearLayout) findViewById(R.id.linLayout);
        View item = ltInflater.inflate(R.layout.item, linLayout, false);
        TextView tvId = (TextView) item.findViewById(R.id.tvId);
        String t = (String) tvId.getText();
        firebaseDB.getBudget(idItem);


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //db.close();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnEdit:
                Log.v(LOG_TAG, "Edit");

                Intent editIntent = new Intent(this, InputActivity.class);
                editIntent.putExtra("idItem", idItem);
                editIntent.putExtra("editItem", "edit");
                editIntent.putExtra("moneyItem", moneyItem);
                editIntent.putExtra("categoryItem", categoryItem);
                editIntent.putExtra("typeItem", typeItem);
                startActivity(editIntent);

                finish();
                break;
            case R.id.btnCopy:
                Log.v(LOG_TAG, "Send money = " + moneyItem);
                Log.v(LOG_TAG, "Send category = " + categoryItem);

               /* Intent copyIntent = new Intent(this, InputActivity.class);
                copyIntent.putExtra("moneyItem", moneyItem);
                copyIntent.putExtra("categoryItem", categoryItem);
                copyIntent.putExtra("typeItem", typeItem);
                startActivity(copyIntent);*/
                finish();
                break;
            case R.id.btnDel:
                firebaseDB.deleteBudget(idItem);
               // db.delRec(Integer.parseInt(idItem), db.DB_TABLE);
                finish();
                break;
            case R.id.btnBack:
                finish();
                break;
        }
    }

    @Override
    public void onGetAllBudgets(Task<QuerySnapshot> task) {

    }

    @Override
    public void onGetBudget(DocumentSnapshot snapshot) {
        String typeStr = (String) snapshot.get(FirebaseDB.DB_BUDGET_COLUMN_TYPE);
        BudgetType type = BudgetType.valueOf(typeStr.toUpperCase());
        String categoryName = (String) snapshot.get(FirebaseDB.DB_BUDGET_COLUMN_CATEGORY_NAME);

        if(type == BudgetType.ADD){
            typeItem = "1";
        } else {
            typeItem = "0";
        }

        categoryItem = categoryName;
    }

    @Override
    public void onGetAllCategories(Task<QuerySnapshot> task) {

    }

    @Override
    public void onGetTotalBalance(Double total) {

    }


}