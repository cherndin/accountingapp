package parivar.accounting;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import parivar.accounting.db.DB;
import parivar.accounting.db.FirebaseDB;
import parivar.accounting.db.TaskComplete;

public class CategoryActivity extends Activity implements View.OnClickListener, TaskComplete {

    private static final String LOG_TAG = "myLogs";

    LinearLayout categoryEditLayout;

    Button btnAddCategory;

    EditText etAddCategory;

    DB db;
    FirebaseDB firebaseDB;

    AlertDialog.Builder ad;
    Context context;

    String idOfCategory;
    String idCategoty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_list_edit);

        //TODO передавать подключени к базе в метод
        /*db = new DB(this);
        db.open();*/
        firebaseDB = new FirebaseDB(this);

        categoryEditLayout = (LinearLayout) findViewById(R.id.categoryEditLayout);

        btnAddCategory = (Button) findViewById(R.id.btnCategoryAdd);
        btnAddCategory.setOnClickListener(this);

        etAddCategory = (EditText) findViewById(R.id.etAddCategory);

        clickToCategory();
        //readCategories();
        firebaseDB.getAllCategories();
    }

    public void readCategories() {

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCategoryAdd:
                if (!etAddCategory.getText().toString().trim().isEmpty()){
                    firebaseDB.addCategory(etAddCategory.getText().toString());
                    firebaseDB.getAllCategories();
                   // db.addCategoryItem(etAddCategory.getText().toString());
                }
                hideKeyboard();
                etAddCategory.setText("");
                readCategories();
                break;
        }
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }


    public void clickToCategory() {

        context = CategoryActivity.this;

        String title = "Remove categories?";
        String message = "Remove?";
        String button1String = "Yes";
        String button2String = "No";

        ad = new AlertDialog.Builder(context);
        ad.setTitle(title);  // заголовок
        ad.setMessage(message); // сообщение
        ad.setPositiveButton(button1String, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                firebaseDB.removeCategory(idCategoty);
               // db.delRec(idCategoty, db.DB_TABLE_CATEGORY);
               // readCategories();
               firebaseDB.getAllCategories();
            }
        });
        ad.setNegativeButton(button2String, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {

            }
        });
        ad.setCancelable(true);
        ad.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {

            }
        });
    }


    @Override
    public void onGetAllBudgets(Task<QuerySnapshot> task) {

    }

    @Override
    public void onGetBudget(DocumentSnapshot snapshot) {

    }

    @Override
    public void onGetAllCategories(Task<QuerySnapshot> task) {
        categoryEditLayout.removeAllViews();

        LayoutInflater ltInflater = getLayoutInflater();

        for (QueryDocumentSnapshot document : task.getResult()) {
            String name = (String)document.get(FirebaseDB.DB_CATEGORY_COLUMN_NAME);
            String id = document.getId();

            Log.v(LOG_TAG, " entering to do");
            final View item = ltInflater.inflate(R.layout.category_item, categoryEditLayout, false);
            TextView tvCategoryItem = (TextView) item.findViewById(R.id.tvCategoryItem);
            tvCategoryItem.setText(name);
            final TextView tvCategoryId = (TextView) item.findViewById(R.id.tvCategoryId);
            tvCategoryId.setText(id);
            item.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;
            categoryEditLayout.addView(item, 0);
            item.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    TextView tvCategoryId = (TextView) v.findViewById(R.id.tvCategoryId);
                    idCategoty = tvCategoryId.getText().toString();
                    ad.show();
                }
            });
        }

    }

    @Override
    public void onGetTotalBalance(Double total) {

    }



    /*@Override
    public void onAddCategory(String name) {

    }*/
}
